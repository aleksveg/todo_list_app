# TODO-LIST APPLIKASJON 

## Formål
Bakgrunnen for dette prosjektet er at det er et arbeidskrav i Systemutviklings-emnet 
for første års dataingeniørstudenter ved NTNU campus Ålesund. 
Prosjektet har som formål å få studentene til å bli kjent med å utvikle et program, 
få kjennskap til ulike systemutviklingsmodeller og benytte disse under prosjektarbeidet.

## Beskrivelse 
Applikasjonen er en gjøremålsliste, som er ment til å benyttes for personer som ønsker 
et digitalt verktøy for å holde oversikt over oppgaver, avtaler, viktige hendelser etc.

I applikasjonen vil man ha muligheten til å opprette en gjøremålsliste, sette starttid og 
sluttid for listen. I listen kan man legge gjøremål som skal utføres i dette tidsrommet. 
Når en oppgave/avtale avsluttet kan man markere denne som utført, og det er også mulig å slette 
enkelte gjøremål eller en hel liste. 

Man kan endre oversikten over listene i to varianter: 
 - det ene er å se det som en liste med punkter
 - det andre er å se det som en kaleder, hvor gjøremålene står markert på datoen de har blitt 
   satt til. 

## Installasjon 
Applikasjonen er en desktop applikasjon, og vil kun være mulig å benytte ved bruk av PC/Mac. 
** Legg ved info om hvordan installere når installasjonsfil er på plass** 


## Support 
Da applikasjonen kun er ment som en prosjektoppgave i dette faget, vil det etter fremføring 
av prosjektet ikke bli gjort endringer/oppdateringer i applikasjonen. 


 
## Bidragsytere 
De som har bidratt til å utvikle applikasjonen er medlemmene av Gruppe 1 - IDATA1002
- Thomas 
- Richileu
- Tor
- Joakim
- Aleksander 

