module no.ntnu.idata1002{
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    requires java.xml;
    requires jakarta.persistence;
    opens no.ntnu.idata1002 to java.fxml;
    exports no.ntnu.idata1002;

}