package no.ntnu.idata1002;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;

/**
 * Represents a pop-up window for adding a
 * TaskList
 */
public class AddTaskListWindowGUI extends Dialog<TaskList> {
    /**
     * Creates an instance of the AddTaskListWindowGUI to get information
     * from user to create a new instance of TaskList
     */
    public AddTaskListWindowGUI() {
        super();
        createContent();
    }

    /**
     * Creates the content of the dialog
     */
    private void createContent() {
        setTitle("Legg til liste");

        // Set the button types
        getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

        int gridWidth = 400;

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 50, 20, 20));
        grid.setMinWidth(gridWidth);

        Label listTitleLabel = new Label("Liste tittel");
        TextField listTitle = new TextField();
        listTitle.setMinWidth(250);

        Label listColor = new Label("Fargen til lista:");
        ObservableList<String> options =
                FXCollections.observableArrayList(
                        "Rød",
                        "Oransje",
                        "Gul",
                        "Lime",
                        "Grønn",
                        "Blå",
                        "Lilla",
                        "Rosa"
                );


        ComboBox comboBox = new ComboBox(options);
        comboBox.setPromptText("Velg farge");

        grid.add(listTitleLabel, 0, 0);
        grid.add(listTitle, 1, 0);
        grid.add(listColor, 0, 1);
        grid.add(comboBox, 1, 1);

        getDialogPane().setContent(grid);

        // Convert the result to TaskList-instance when OK button is clicked
        setResultConverter(
                (ButtonType button) -> {
                    TaskList result = null;
                    if (button == ButtonType.OK) {
                      if(comboBox.getValue() != null){
                        String title = listTitle.getText();
                        if (title != null || !title.isBlank()) {
                          String color = "";
                          if (comboBox.getValue() instanceof String) {
                            color = (String) comboBox.getValue();

                            switch (color) {
                              case "Rød":
                                color = "red";
                                break;

                              case "Oransje":
                                color = "orange";
                                break;

                              case "Gul":
                                color = "yellow";
                                break;

                              case "Lime":
                                color = "lime";
                                break;

                              case "Grønn":
                                color = "green";
                                break;

                              case "Blå":
                                color = "blue";
                                break;

                              case "Lilla":
                                color = "purple";
                                break;

                              case "Rosa":
                                color = "pink";
                                break;
                            }
                            result = new TaskList(title, color);
                      }
                            }

                        } else {
                        Alert selecteColorAlert = new Alert(Alert.AlertType.INFORMATION);
                        selecteColorAlert.setTitle("Ingen farge valgt");
                        selecteColorAlert.setHeaderText("Du må velge en farge før du kan opprette " +
                          "en liste.");
                        selecteColorAlert.showAndWait();
                      }
                    }
                    return result;
                }
        );
    }
}
