package no.ntnu.idata1002;


public class CalenderController {

    /**
     * Calculates the next month.
     *
     * @param currentMonth the current month displayed in the calender
     *                     view.
     * @param currentYear the current year displayed in the calender
     *                    view.
     * @return a list of integer where the first entry is the next month
     * and the second entry is the next year
     */
    public Integer[] nextMonth(int currentMonth, int currentYear) {
        int nextMonth = 0;
        int nextYear = 0;

        if (currentMonth == 12) {
            nextMonth = 1;
            nextYear = currentYear + 1;
        } else {
            nextMonth = currentMonth + 1;
            nextYear = currentYear;
        }

        Integer[] values = {nextMonth, nextYear};

        return values;
    }

    /**
     * Calculates the previous month.
     *
     * @param currentMonth the current month displayed in the calender
     *                     view.
     * @param currentYear the current year displayed in the calender
     *                    view.
     * @return a list of integer where the first entry is the previous month
     * and the second entry is the previous year.
     */
    public Integer[] previousMonth(int currentMonth, int currentYear) {
        int previousMonth = 0;
        int previousYear = 0;

        if (currentMonth == 1) {
            previousMonth = 12;
            previousYear = currentYear - 1;
        } else {
            previousMonth = currentMonth - 1;
            previousYear = currentYear;
        }

        Integer[] values = {previousMonth, previousYear};

        return values;
    }
}
