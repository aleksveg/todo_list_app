package no.ntnu.idata1002;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents the gui Node in the application with header
 * and grid. Header consists of a next and previous button
 * as well as a title with the current month and year being displayed
 * in the calender view
 *
 * To create a calender object call its constructor where the
 * parameter is the parent calling the constructor.
 *
 * Every time a change is made to the calender or a list
 * the "updateCalender"-method should be called to make
 * sure the changes are up to date in the calender view
 */
public class CalenderGui {

    // The root node of the calender.
    private VBox calenderRoot;

    // A list of all VBoxes in the grid of the calender.
    private GridPane calenderGrid;
    private ArrayList<VBox> gridVBoxes;

    // The title of the month and year displayed at the top of the calender
    private HBox calenderHeader;

    // The controller for the calender
    private CalenderController controller;

    // Fields for keeping track of the current month being displayed
    // on the calender
    private int yearDisplayed;
    private int monthDisplayed;

    private ToDoGUI parent;

    private TaskList selectedTaskList;

    // Styling
    private final static String buttonStyle = "-fx-background-color:#800000;-fx-text-fill:#fff";
    private final static String background = "-fx-background-color:#f2f2f2";
    private final static String textColor = "-fx-text-fill:#000";

    /**
     * Creates an instance of the calender view
     *
     * @param parent the object calling this constructor.
     *               Store in a field in this class so it can
     *               be used later to call method in the parent
     *               class.
     */
    public CalenderGui(ToDoGUI parent) {

        // Initialize the controller for the calender
        this.controller = new CalenderController();

        // Initialize the calender root node
        this.calenderRoot = new VBox();

        // Initialize the list holding every grid element
        this.gridVBoxes = new ArrayList<>();

        // Initialize the year and month being displayed.
        // When a calender object is created, the current month and year
        // being display will be the current month and year of the time its
        // created.
        this.yearDisplayed = LocalDateTime.now().getYear();
        this.monthDisplayed = LocalDateTime.now().getMonthValue();

        // Create the header of the calender
        this.calenderHeader = createCalenderHeader();

        // Create the calender grid
        this.calenderGrid = createCalenderGrid(this.monthDisplayed, this.yearDisplayed);

        // Add every component to the root node
        this.calenderRoot.getChildren().addAll(calenderHeader, calenderGrid);
        this.calenderRoot.setStyle(background);
        this.calenderRoot.setPadding(new Insets(20, 0, 0, 20));

        this.parent = parent;
    }

    /**
     * Creates the header of the calender with a next and previous
     * buttons as well as the title consisting of the month and the
     * year being displayed in the calender view.
     *
     * @return a HBox with all the elements in the calender
     * header.
     */
    private HBox createCalenderHeader() {
        HBox header = new HBox();
        header.setPadding(new Insets(10,0,50,0));
        header.setAlignment(Pos.CENTER);

        Button previous = new Button("Forrige");
        previous.setStyle(buttonStyle);
        previous.setCursor(Cursor.HAND);
        previous.setOnAction(event -> {
            Integer[] date = this.controller.previousMonth(this.monthDisplayed, this.yearDisplayed);
            this.monthDisplayed = date[0];
            this.yearDisplayed = date[1];
            this.calenderGrid = createCalenderGrid(monthDisplayed, yearDisplayed);
            this.calenderHeader = createCalenderHeader();
            this.updateCalenderRoot();
            updateCalender();
        });

        Button next = new Button("Neste");
        next.setStyle(buttonStyle);
        next.setCursor(Cursor.HAND);
        next.setOnAction(event -> {
            Integer[] date = this.controller.nextMonth(this.monthDisplayed, this.yearDisplayed);
            this.monthDisplayed = date[0];
            this.yearDisplayed = date[1];
            this.calenderGrid = createCalenderGrid(monthDisplayed, yearDisplayed);
            this.calenderHeader = createCalenderHeader();
            this.updateCalenderRoot();
            updateCalender();
        });

        Label calenderTitle = new Label(getNameOfMonth(this.monthDisplayed) + " " + this.yearDisplayed);
        calenderTitle.setMinWidth(200);
        calenderTitle.setPadding(new Insets(5,20,0,20));
        calenderTitle.setStyle(textColor);
        calenderTitle.setAlignment(Pos.CENTER);

        header.getChildren().addAll(previous, calenderTitle, next);

        return header;
    }

    /**
     * Updates the calender root. Call this method after a change
     * has been made to the calender.
     */
    private void updateCalenderRoot() {
        this.calenderRoot.getChildren().clear();
        this.calenderRoot.getChildren().addAll(this.calenderHeader, this.calenderGrid);
    }

    /**
     * Checks if the task given as parameter has a deadline
     * in the current month being displayed in the calender.
     * If so the method returns {@code true} and if not
     * the method returns {@code false}.
     *
     * @param task the task to check for
     * @return {@code true} if task is in the current month
     * being displayed and {@code false} if not
     */
    private boolean checkIfTaskInCurrentMonth(Task task) {
        boolean taskInCurrentMonth = false;

        int month = task.getDeadline().getMonthValue();
        int year = task.getDeadline().getYear();

        if (month == this.monthDisplayed && year == this.yearDisplayed) {
            taskInCurrentMonth = true;
        }

        return taskInCurrentMonth;
    }

    /**
     * Returns the name of the month for the month given as
     * parameter.
     *
     * @param month the month to get the name of. Passed as an
     *              integer where January = 0, and Desember = 11
     * @return a strin with the name of the month
     */
    private String getNameOfMonth(int month) {
        String nameOfMonth = "";

        String[] months = {"Januar", "Februar", "Mars", "April", "Mai", "Juni", "July", "August", "September",
                "Oktober", "November", "Desember"};

        nameOfMonth = months[month - 1];

        return nameOfMonth;
    }

    /**
     * Creates the calender grid with days for the month that should be displayed.
     * For example January should display 31 days, while February should only display
     * 28 (29) days.
     *
     * @param month the month to be displayed
     * @param year the year to be displayed
     * @return a GridPane with all the dates for the month
     */
    private GridPane createCalenderGrid(int month, int year) {
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);

        int index = 0;
        int column = 0;
        int row = 0;
        int daysInMonth = getDaysInMonth(month, year);
        while (index < daysInMonth) {
            Label label = new Label("" + (index + 1));
            label.setStyle(textColor);
            VBox vbox = new VBox(label);
            vbox.setMinHeight(99);
            vbox.setMinWidth(100);
            vbox.setAlignment(Pos.TOP_CENTER);
            grid.add(vbox, column, row);
            this.gridVBoxes.add(vbox);
            column++;
            if (column == 7) {
                column = 0;
                row++;
            }
            index++;
        }

        return grid;
    }

    /**
     * Returns the number of days in the current month
     *
     * @param month the month to get number of days from
     * @param year the year to get number of days from. Used to calculate
     *             if the current year os a leap year or not
     * @return int with the number of days in the month and year specified
     * in the parameter.
     */
    private int getDaysInMonth(int month, int year) {
        int daysInMonth = 0;

        if (year % 4 == 0) {    // Leap year
            Integer[] days = {31,29,31,30,31,30,31,31,30,31,30,31};
            daysInMonth = days[month - 1];
        } else {                // Not leap year
            Integer[] days = {31,28,31,30,31,30,31,31,30,31,30,31};
            daysInMonth = days[month - 1];
        }

        return daysInMonth;
    }


    /**
     * Returns the list of all gridBoxes in the calender.
     * This list can be used to get one of the boxes and add
     * elements to it.
     *
     * @return a list with all VBoxes in the calender grid.
     */
    public List<VBox> getGridBoxes() {
        return this.gridVBoxes;
    }

    /**
     * Returns a VBox that can be displayed in the main application
     *
     * @param selectedTaskList the selected list in the main
     *                         application that should be displayed
     *                         in the calender
     * @return a vbox to be displayed in the main application
     */
    public VBox displayCalender(TaskList selectedTaskList) {
        if (selectedTaskList != null) {
            this.selectedTaskList = selectedTaskList;
        } else {
            try {
                this.selectedTaskList.getTasks().clear();
            } catch (NullPointerException e) {
                System.out.println("No selected task list");
            }
        }
        updateCalender();

        return this.calenderRoot;
    }

    /**
     * Updates the calender view. Call this method whenever a changed
     * is made to a list to make sure the calender view is up to date.
     */
    private void updateCalender() {
        this.calenderRoot.getChildren().clear();
        this.gridVBoxes.clear();
        this.calenderGrid.getChildren().clear();
        this.calenderHeader.getChildren().clear();

        this.calenderHeader = createCalenderHeader();
        this.calenderGrid = createCalenderGrid(this.monthDisplayed, this.yearDisplayed);
        this.addTasks();
        this.calenderRoot.getChildren().addAll(calenderHeader, calenderGrid);
    }

    /**
     * Adds all the tasks in the selected task list to
     * the calender view.
     */
    private void addTasks() {
        if (this.selectedTaskList != null) {
            for (Task task : this.selectedTaskList.getTasks()) {
                if (checkIfTaskInCurrentMonth(task)) {
                    // Get det deadline day of the task
                    int deadlineDay = task.getDeadline().getDayOfMonth();
                    // Get the vbox of that day
                    VBox vbox = this.gridVBoxes.get(deadlineDay - 1);
                    // Create the text that will be added to the vbox
                    Label text = new Label(task.getTitle());
                    //text.setEditable(false);
                    // Set style to the text
                    String textStyle = "-fx-text-fill:" + this.selectedTaskList.getColor();
                    text.setStyle(textStyle);
                    text.setFont(new Font(13));
                    text.setCursor(Cursor.HAND);
                    // Display task details when user click on the task
                    text.setOnMouseClicked(mouseEvent -> parent.doShowDetails(task));
                    // Add text to vbox
                    vbox.getChildren().addAll(text);
                }
            }
        }
    }
}
