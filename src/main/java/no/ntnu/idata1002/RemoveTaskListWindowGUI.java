package no.ntnu.idata1002;



import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Represents a pop-up window for removing
 * a TaskList
 */
public class RemoveTaskListWindowGUI extends Dialog<TaskList> {
    /**
     * Creates an instance of the RemoveTaskListWindowGUI to
     * get information from user to remove a TaskList
     *
     * @param taskListArchive the archive of all list.
     *                        The list will be removed from
     *                        this archive
     */
    public RemoveTaskListWindowGUI(TaskListArchive taskListArchive) {
        super();
        createContent(taskListArchive);
    }

    /**
     * Creates the content for the dialog
     */
    private void createContent(TaskListArchive taskListArchive) {
        setTitle("Fjern liste");

        // Set the button types
        getDialogPane().getButtonTypes()
                .addAll(ButtonType.OK, ButtonType.CANCEL);

        // Create grid and set grid style
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 50, 20, 20));
        grid.setMinWidth(400);

        // Create label text
        Label label = new Label("Fjern Liste: ");

        // Create a list containing a string for
        // each TaskList in the TaskListArchive.
        // The string is set to be the title
        // of the TaskList
        List<String> taskListNames = taskListArchive
                .getArchive()
                .stream()
                .map(taskList -> taskList.getTitle())
                .collect(Collectors.toList());
        // User the list above to create options
        // for a roll-down menu
        ObservableList<String> options =
                FXCollections.observableArrayList(taskListNames);

        // Create a roll-down menu with options from
        // above
        ComboBox comboBox = new ComboBox(options);
        comboBox.setPromptText("Velg en liste");

        // Add all elements to the grid
        grid.add(label, 0, 0);
        grid.add(comboBox, 1, 0);

        // Add grid to dialog
        getDialogPane().setContent(grid);

        // Convert the result to a TaskList and
        // return it
        setResultConverter(
                (ButtonType button) -> {
                    TaskList result = null;
                    if (button == ButtonType.OK) {
                        String title = "";
                        if (comboBox.getValue() instanceof String) {
                            title = (String) comboBox.getValue();
                        }
                        result = taskListArchive.getTaskListByName(title);
                    }
                    return result;
                }
        );
    }
}
