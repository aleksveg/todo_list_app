package no.ntnu.idata1002;

/**
 * This Class represents a tag to put on a given task
 * The tag is used to mark the priority of a given task
 *
 * @author aleksander / gruppe1
 * @version 1.0
 */

public class Tag {

    private String title; // to hold the name of the title

    public Tag(String title) {
        if (title == null || title.isBlank()) { // check if nothing entered or title input is empty string
            throw new IllegalArgumentException("No value entered");
        } else {
            this.title = title;
        }

    }

    /**
     * Returns the title.
     * @return title - title to be returned.
     */
    public String getTitle() {
        return this.title;
    }
}
