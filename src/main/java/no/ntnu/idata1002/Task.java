package no.ntnu.idata1002;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Allows you to create new task
 * The class creates a new object that can be added to lists and shown to user in the gui
 * It uses the Tag class to allow user to add new tags to the task to help the search for it
 *
 * Includes get and set for the different variables
 *
 * This class has two constructors one if the the task will have a deadline one without a deadline
 *
 * it contains an arraylist to store the tags in the class
 *
 * @author Tor Oveland Gikling - Gruppe 1
 * @version 1.0
 */
public class Task {
    private String title;
    private String description;
    private final LocalDateTime creationDate;
    private LocalDateTime deadline;
    private String author;
    private boolean completed;
    private int priority;
    private boolean recurring;
    private ArrayList<Tag> tagList;
    private String listTitle; // Title of the list the task is in

    /**
     * The constructor for the class with deadline
     * If the task has a deadline this constructor will be called
     * @param title The name of the task
     * @param description The description of the task can be left empty
     * @param author The person who created the task
     * @param deadline The date and time the task i supposed to be finished by
     * @param priority The priority of the Task where 1 is the highest and 5 is lowest.
     * @param recurring asks if the remove class
     */
    public Task(String title, String description, String author, LocalDateTime deadline  , int priority, boolean recurring) {

        if (title == null) {
            throw new IllegalArgumentException("The name of the task is NULL");
        }
        if (description == null) {
            throw new IllegalArgumentException("The description is NULL");
        }
        if (deadline == null) {
            throw new IllegalArgumentException("The deadline is NULL");
        }
        if (author == null) {
            throw new IllegalArgumentException("The author is NULL");
        }
        if (priority <=0 || priority >5) {
            throw new IllegalArgumentException("The priority is less or equal to 0");
        }

        if (title.isBlank()) {
            this.title = "Ny Oppgave";
        } else {
            this.title = title;
        }
        if (description.isBlank()) {
            this.description = "";
        } else {
            this.description = description;
        }

        this.creationDate = LocalDateTime.now();
        this.deadline = deadline;
        this.author = author;
        this.completed = false;
        this.priority = priority;
        this.recurring = recurring;
        this.listTitle = "";

        this.tagList = new ArrayList<Tag>();
    }

    /**The constructor for the class without deadline
     * If the task has noo deadline this constructor will be called
     * @param title The name of the task
     * @param description The description of the task can be left empty
     * @param author The person who created the task
     * @param priority The priority of the Task where 1 is the highest and 5 is lowest.
     * @param recurring asks if the remove class
     */
    public Task(String title, String description, String author, int priority, boolean recurring) {

        if (title == null) {
            throw new IllegalArgumentException("The name of the task is NULL");
        }
        if (description == null) {
            throw new IllegalArgumentException("The description is NULL");
        }
        if (author == null) {
            throw new IllegalArgumentException("The author is NULL");
        }
        if (priority <=0 || priority >5) {
            throw new IllegalArgumentException("The priority is less or equal to 0");
        }

        this.title = title;
        this.description = description;
        this.creationDate = LocalDateTime.now();
        this.author = author;
        this.completed = false;
        this.priority = priority;
        this.recurring = recurring;
        this.listTitle = "";

        this.tagList = new ArrayList<Tag>();
    }

    /**
     * The constructor for the class with deadline
     * If the task has a deadline this constructor will be called
     * @param title The name of the task
     * @param description The description of the task can be left empty
     * @param author The person who created the task
     * @param deadline The date and time the task i supposed to be finished by
     * @param priority The priority of the Task where 1 is the highest and 5 is lowest.
     * @param recurring asks if the remove class
     * @param listTitle The title of the list the task is added to
     */
    public Task(String title, String description, String author, LocalDateTime deadline  , int priority,
                boolean recurring, String listTitle) {

        if (title == null) {
            throw new IllegalArgumentException("The name of the task is NULL");
        }
        if (description == null) {
            throw new IllegalArgumentException("The description is NULL");
        }
        if (deadline == null) {
            throw new IllegalArgumentException("The deadline is NULL");
        }
        if (author == null) {
            throw new IllegalArgumentException("The author is NULL");
        }
        if (priority <=0 || priority >5) {
            throw new IllegalArgumentException("The priority is less or equal to 0");
        }
        if (listTitle == null || listTitle.isBlank()) {
            throw new IllegalArgumentException("List title is not set");
        }

        if (title.isBlank()) {
            this.title = "Ny Oppgave";
        } else {
            this.title = title;
        }
        if (description.isBlank()) {
            this.description = "";
        } else {
            this.description = description;
        }

        this.creationDate = LocalDateTime.now();
        this.deadline = deadline;
        this.author = author;
        this.completed = false;
        this.priority = priority;
        this.recurring = recurring;
        this.listTitle = listTitle;

        this.tagList = new ArrayList<Tag>();
    }

  /**
   * Creates an instance of a task with specified creation date and completed state.
   * NB: Should only be used when creating task lists from a parsed XML file.
   * @param title of the task
   * @param description of the task
   * @param author of the task
   * @param deadline of the task
   * @param creationDate of the task
   * @param priority of the task
   * @param recurring state of the task
   * @param completed state of the task
   */
  public Task(String title, String description, String author, LocalDateTime deadline,
              LocalDateTime creationDate ,int priority, boolean recurring, boolean completed){
    if (title == null) {
      throw new IllegalArgumentException("The name of the task is NULL");
    }
    if (description == null) {
      throw new IllegalArgumentException("The description is NULL");
    }
    if (deadline == null) {
      throw new IllegalArgumentException("The deadline is NULL");
    }
    if (creationDate == null) {
      throw new IllegalArgumentException("The creationDate is NULL");
    }
    if (author == null) {
      throw new IllegalArgumentException("The author is NULL");
    }
    if (priority <=0 || priority >5) {
      throw new IllegalArgumentException("The priority is less or equal to 0");
    }

    if (title.isBlank()) {
      this.title = "Ny Oppgave";
    } else {
      this.title = title;
    }
    if (description.isBlank()) {
      this.description = "";
    } else {
      this.description = description;
    }

    this.title = title;
    this.description = description;
    this.author = author;
    this.deadline = deadline;
    this.creationDate = creationDate;
    this.priority = priority;
    this.recurring = recurring;
    this.completed = completed;

  }

    /**
     * Gets the title of the task
     * @return the title of the task
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * Allows you to change the title of the task
     * It checks if the title is null if that`s it throws an IllegalArgumentException
     * @param title The title of the task
     */
    public void setTitle(String title) {
        if (title == null) {
            throw new IllegalArgumentException("The title is NULL");
        }
        this.title = title;
    }

    /**
     * Gets the description of the task
     * @return Returns the description of the task
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the decription incase the user wants to add to it
     * or change it
     * @param description The description of the task
     */
    public void setDescription(String description) {
        if (description == null) {
            throw new IllegalArgumentException("The description is NULL");
        }
        this.description = description;
    }

    /**
     * Gets the local time of the task
     * @return returns the date and time of the creation of the task
     */
    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    /**
     * get the deadline time of the task
     * @return The deadline time of the task
     */
    public LocalDateTime getDeadline() {
        return deadline;
    }

    /**
     * Sets the deadline for the task if it needs to be changed or you wish to set one
     * @param deadline The deadline of the task with the Datatype LocalDateTime
     */
    public void setDeadline(LocalDateTime deadline) {
        if (deadline == null) {
            throw new IllegalArgumentException("The deadline date is Null");
        }
        this.deadline = deadline;
    }

    /**
     * Gets the name of the creator of the task
     * @return The name of the creator of the task
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Changes the name of the author of the task in case it`s wrong
     * It takes a String datatype
     * If the author is null it will throw an IllegalArgumentException
     * @param author The name of the creator of the task with the datatype String
     */
    public void setAuthor(String author) {
        if (author == null) {
            throw new IllegalArgumentException("The author is null");
        }
        this.author = author;

    }

    /**
     * Gets the completed status of the task
     * @return Returns the completed status of the task with true or false
     */
    public boolean getCompleted() {
        return completed;
    }

    /**
     * Changes the status of completed of a task
     * @param completed enter true or false of the completion of the task datatype boolean
     */
    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    /**
     * Gets the priority of the task between 1 and 5
     * @return returns the priority of the task as an int between 1 and 5 with 1 as the highest
     */
    public int getPriority() {
        return priority;
    }

    /**
     * Sets the priority of the task
     * Includes a test for the value of the int.
     * If the int is <= 0 or greater than 5 an illegal argument exception is
     * @param priority The priority of the task from 1 to 5
     */
    public void setPriority(int priority) {
        if (priority <=0|| priority > 5) {
            throw new IllegalArgumentException("The priority is lesser or equal to 0 or greater than 5");
        }
        this.priority = priority;
    }

    /**
     * get if the task is recurring
     * @return Returns the boolean value of if the task is recurring
     */
    public boolean getRecurring() {
        return recurring;
    }

    /**
     * Sets if the task is recurring
     * @param recurring true or false value of a boolean
     */
    public void setRecurring(boolean recurring) {
        this.recurring = recurring;
    }

    /**
     * Searches for the tag is tagList. Will be accessed by taskList and from there
     * the GUI
     * @param tagTitle A String value of the name of the tag to search for in the tagList
     * @return a boolean value of true if the tag is found false if the tag is not found
     */
    public boolean searchTagByTitle(String tagTitle) {

        Iterator<Tag> it = tagList.iterator();

        boolean success = false;

        while (it.hasNext() && !success) {
            Tag tag = it.next();
            if (tag.getTitle().equalsIgnoreCase(tagTitle)) {
                success = true;
            } else {
                success = false;
            }
        }
        return success;
    }

    /**
     * Adds a tag using the Tag class
     * Checks if the title of the tag is blank or null before creating a new Tag class
     * Then adds the tag to an arrayList that keeps the tags so they can be used to searched for
     * from the UI
     * @param title The title of the tag that will be sent to the constructor of the tag class
     */
    public void addTag(String title) {
        if(title == null || title.isBlank()) {
            throw new IllegalArgumentException("The tagTitle is blank or null");
        }
        Tag tag = new Tag(title);

        this.tagList.add(tag);

    }

    public ArrayList<Tag> getTagList() {
        return this.tagList;
    }

    /**
     * Removes a tag from the tagList and subsequently from the task
     * @param toDelete The name of the tag you want to delete
     * @return a boolean of true of false to indicate if the tag has been removed or not
     */
    public boolean removeTag(String toDelete) {
        Iterator<Tag> it = tagList.iterator();
        boolean success = false;

        while (it.hasNext() && !success) {
            Tag tag = it.next();

            if (tag.getTitle().equalsIgnoreCase(toDelete)) {
                this.tagList.remove(tag);
                success = true;
            }else {
                success = false;
            }
        }
        return success;
    }

}


