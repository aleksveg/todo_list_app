package no.ntnu.idata1002;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;


/**
 * A dialog used to get the necessary information from the user about a new Task,
 * to be able to create a new Task object, and add it to the chosen TaskList.
 *
 * @author Thomas
 */
public class TaskDetailsGUI extends Dialog<TaskStringPair> {

  /**
   * The mode of the dialog. If the dialog is opened to edit and existing Task,
   * the mode should be set to <code>Mode.EDIT</code>. If it is opened to create a new
   * Task, the mode should be set to <code>Mode.NEW</code>.
   * Note that if mode is <code>Mode.EDI</code>T,
   * the returned TaskStringPair will have an empty string.
   */
  public enum Mode {
    NEW, EDIT
  }

  /**
   * The mode of the dialog. NEW if new Task, EDIT if editing Task.
   */
  private final Mode mode;

  /**
   * The taskLists available in the application when the dialog is created.
   */
  private ArrayList<TaskList> availableTaskLists;
  /**
   * The task currently selected when the dialog is created. Used in Edit mode.
   */
  private Task selectedTask;

  /**
   * Constructor for the AddTaskWindowGUI class that creates a new Task.
   * @param t An ArrayList of TaskLists used to generate combobox from which
   *          user selects the TaskList to add the new Task to.
   */
  public TaskDetailsGUI(ArrayList<TaskList> t){
    super();
    this.availableTaskLists = t;
    mode = Mode.NEW;
    createContent();
  }

  /**
   * Consctuctor for TaskDetailsGUI for editing an existing Task.
   * @param t The Task that should be edited.
   */
  public TaskDetailsGUI(Task t){
    super();
    this.selectedTask = t;
    mode = Mode.EDIT;
    createContent();
  }

  /**
   * Generates the dialog window the user will use to add new tasks to the app.
   */
  private void createContent(){
    if(mode == Mode.NEW){
      setTitle("Ny oppgave");
    } else if (mode == Mode.EDIT){
      setTitle("Oppgavedetaljer");
    }

    //Set button types
    getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

    GridPane grid = new GridPane();
    grid.setHgap(10);
    grid.setVgap(10);
    grid.setPadding(new Insets(20, 150, 10, 10));

    //Creating title component.
    TextField titleTf = new TextField();
    titleTf.setPromptText("Tittel...");

    //Creating author component
    TextField authorTf = new TextField();
    authorTf.setPromptText("Forfatter...");

    //Creating targetLists component, only if mode == Mode.NEW
    ComboBox targetListsCb = null;
    if(mode == Mode.NEW){
      //Creating target list components.
      //Generating list of taskList titles based on availableTaskLists field.
      List<String> taskListNames = availableTaskLists.stream()
          .map(taskList -> taskList.getTitle()).collect(Collectors.toList());
      //Making observable list of taskList titles to feed to ComboBox.
      ObservableList<String> observableTaskListNames =
          FXCollections.observableArrayList(taskListNames);

      targetListsCb = new ComboBox(observableTaskListNames);
      targetListsCb.setPromptText("Velg Liste");


    }

    //Creating deadline components.
    DatePicker deadLineDp = new DatePicker();
    deadLineDp.setPromptText("Deadline...");
    //Disabling past dates in deadLineDp
    deadLineDp.setDayCellFactory(picker -> new DateCell() {
      public void updateItem(LocalDate date, boolean empty) {
        super.updateItem(date, empty);
        LocalDate today = LocalDate.now();

        setDisable(empty || date.compareTo(today) < 0);
      }
    });

    deadLineDp.setMaxWidth(100);


    ArrayList<Integer> hours = new ArrayList<>();
    for (int i = 0; i < 24; i++) {
      hours.add(i);
    }
    ObservableList<Integer> observableHours =
            FXCollections.observableArrayList(hours);
    ComboBox hourPickerCb = new ComboBox(observableHours);
    hourPickerCb.setPromptText("Time");
    hourPickerCb.setMaxWidth(70);


    ArrayList<Integer> minutes = new ArrayList<>();
    for (int i = 0; i < 60; i++) {
      minutes.add(i);
    }
    ObservableList<Integer> observableMinutes =
            FXCollections.observableArrayList(minutes);

    ComboBox minutePickerCb = new ComboBox(observableMinutes);
    minutePickerCb.setMaxWidth(80);
    minutePickerCb.setPromptText("Minutt");



    //Creating priority components
    List<Integer> priorities = Arrays.asList(1,2,3,4,5);
    ObservableList<Integer> observablePriorities =
        FXCollections.observableArrayList((priorities));
    ComboBox priorityCb = new ComboBox(observablePriorities);

    priorityCb.setPromptText("Velg prioritet");


    //Creating description components.
    TextArea descriptionTa = new TextArea();
    descriptionTa.setPromptText("Beskrivelse...");

    //Creating "Task.completed" component.
    //Should only be shown if mode == Mode.EDIT
    CheckBox taskCompletedChb = new CheckBox("Fullført");
    if(mode == Mode.EDIT){
      taskCompletedChb.setSelected(selectedTask.getCompleted());
    }

    //Adding existing info on task, only if mode == Mode.EDIT
    if(mode == Mode.EDIT){
      titleTf.setText(selectedTask.getTitle());
      authorTf.setText(selectedTask.getAuthor());
      hourPickerCb.setValue(hours.get(selectedTask.getDeadline().getHour()));
      minutePickerCb.setValue(hours.get(selectedTask.getDeadline().getMinute()));

      //Processing Task's date data
      String taskDateString = selectedTask.getDeadline().getDayOfMonth() +
          "." + selectedTask.getDeadline().getMonthValue() +
          "." +  selectedTask.getDeadline().getYear();
      DateTimeFormatter dateFormatter;
      if(selectedTask.getDeadline().getMonthValue() > 9){
        dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        if(selectedTask.getDeadline().getDayOfMonth() > 9){
          dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        } else {
          dateFormatter= DateTimeFormatter.ofPattern("d.MM.yyyy");
        }
      } else {
        dateFormatter = DateTimeFormatter.ofPattern("dd.M.yyyy");
        if (selectedTask.getDeadline().getDayOfMonth() <= 9) {
          dateFormatter = DateTimeFormatter.ofPattern("d.M.yyyy");
        } else {
          dateFormatter = DateTimeFormatter.ofPattern("d.M.yyyy");
        }
      }
      deadLineDp.setValue(LocalDate.parse(taskDateString, dateFormatter));
      

      //Setting Task priority
      priorityCb.getSelectionModel().select(1);
      switch (selectedTask.getPriority()){
        case 1:
          priorityCb.getSelectionModel().select(0);
          break;
        case 2:
          priorityCb.getSelectionModel().select(1);
          break;
        case 3:
          priorityCb.getSelectionModel().select(2);
          break;
        case 4:
          priorityCb.getSelectionModel().select(3);
          break;
        case 5:
          priorityCb.getSelectionModel().select(4);
          break;
      }

      descriptionTa.setText(selectedTask.getDescription());
    }


    grid.add(new Label("Tittel:"), 0,0);
    grid.add(titleTf, 1, 0, 2, 1);
    grid.add(new Label("Forfatter"), 0, 2);
    grid.add(authorTf, 1, 2, 2, 1);
    if(targetListsCb != null){
      grid.add(new Label("Liste:"), 0,1);
      grid.add(targetListsCb, 1,1, 2,1);
    }
    grid.add(new Label("Deadline:"), 0, 3);
    grid.add(deadLineDp, 1,3, 1,1);
    grid.add(new Label("Time (0-23)"),0,4);
    grid.add(hourPickerCb, 1, 4);
    grid.add(new Label("Minutt (0-59)"), 2,4);
    grid.add(minutePickerCb, 3,4);
    grid.add(new Label("Prioritet (1-5)"), 0,5);
    grid.add(priorityCb, 1, 5);
    grid.add(new Label("Beskrivelse:"), 4, 0);
    grid.add(descriptionTa, 4, 1, 1, 2);
    if(mode == Mode.EDIT){
      grid.add(taskCompletedChb, 5,1);
    }
    //grid.setGridLinesVisible(true);

    getDialogPane().setContent(grid);

    // Convert the result to TaskTaskListPair-instance when the OK button is clicked.
    ComboBox finalTargetListsCb = targetListsCb;
    setResultConverter(
        (ButtonType button) -> {
          TaskStringPair fullResult = null;
          Task resultTask = null;
          String resultTaskListName = null;

          if(button == ButtonType.OK) {
              //Check if there are any fields that are not filled.
            if(titleTf.getText().isEmpty() || descriptionTa.getText().isEmpty() ||
                authorTf.getText().isEmpty() || deadLineDp.getValue() == null ||
                hourPickerCb.getValue() == null || minutePickerCb.getValue() == null){
              Alert alert = new Alert(Alert.AlertType.INFORMATION);
              alert.setTitle("Nødvendig informasjon mangler!");
              alert.setHeaderText("Du må fylle ut nødvendig informasjon.");
              alert.setContentText("Alle felt må fylles ut." +
                  " Ingen oppgave har blitt opprettet.");
              alert.showAndWait();
            } else {
              //ToDo Add conversion/creation of Task object here.
              String newTitle = titleTf.getText();
              String newDescription = descriptionTa.getText();
              String newAuthor = authorTf.getText();
              int newPriority = Integer.parseInt(priorityCb.getValue().toString());
              //Todo datetime logic
              if(deadLineDp.getValue() != null){
                int year = deadLineDp.getValue().getYear();
                int day = deadLineDp.getValue().getDayOfMonth();
                int month = deadLineDp.getValue().getMonthValue();

                int hourInt = Integer.parseInt(hourPickerCb.getValue().toString());
                int minuteInt = Integer.parseInt(minutePickerCb.getValue().toString());

                LocalDateTime newLtd = LocalDateTime.of(year, month, day,
                    hourInt, minuteInt);
                //Generating new task based on dialog input.
                resultTask = generateTaskFull(newTitle, newDescription,
                    newAuthor, newLtd, newPriority, false);
              } else {
                //Generating new task without deadline based on dialog input.
                resultTask = generateTaskWODeadline(newTitle, newDescription, newAuthor,
                    newPriority, false);
              }
              if(mode == Mode.EDIT){
                resultTask.setCompleted(taskCompletedChb.isSelected());
              }

              if(mode == Mode.NEW){
                resultTaskListName = finalTargetListsCb.getValue().toString();
              } else if (mode == Mode.EDIT){
                resultTaskListName = "";
              }

              fullResult = new TaskStringPair(resultTask, resultTaskListName);
            }
          }
          return fullResult;
        }
    );
  }


  /**
   * Generates a new Task object based on the information sent in as parameters.
   * @param title The new tasks title.
   * @param description The new tasks description.
   * @param author The new tasks author.
   * @param deadline The new tasks deadline.
   * @param priority The new tasks priority.
   * @param recurring True if task should be recurring, else false.
   * @return The newly generated Task object.
   */
  private Task generateTaskFull(String title, String description, String author,
                                LocalDateTime deadline, int priority, boolean recurring)
  {
    return new Task(title, description, author, deadline, priority, recurring);
  }

  /**
   * Generates a new Task object based on the information sent in as parameters,
   * without a deadline.
   * @param title The new tasks title.
   * @param description The new tasks description.
   * @param author The new tasks author.
   * @param priproty The new tasks priority.
   * @param recurring True if task should be recurring, else false.
   * @return The newly generated Task object.
   */
  private Task generateTaskWODeadline(String title, String description, String author,
                                      int priproty, boolean recurring)
  {
    return new Task("", "" , "", priproty, false);
  }
}
