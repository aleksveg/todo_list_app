package no.ntnu.idata1002;

/**
 * Represents a list in the todo list application.
 * A list has a title, a color and a list of tasks.
 * The list also keeps track of when it was created and if every
 * task in the list is completed or not.
 *
 * @author Joakim
 * @version 1.0
 */

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

public class TaskList {
    private String title;
    private final LocalDateTime creationDate;
    private boolean completed;
    private ArrayList<Task> tasks;
    private String color;

    /**
     * Creates an instance of a task list
     *
     * @param title of the list
     * @param color for the list
     */
    public TaskList(String title, String color)
    {
        if (color == null) {
            color = "";
        }
        if (color.isBlank() || title == null) {
            throw new IllegalArgumentException("Null or empty parameter");
        }

        this.title = title;
        this.creationDate = LocalDateTime.now();
        this.completed = false;
        this.tasks = new ArrayList<>();
        this.color = color;
    }

  /**
   * Creates an instance of a task list with specified creation date and completed state.
   * NB: Should only be used when creating task lists from a parsed XML file.
   * @param title of the list
   * @param color for the list
   * @param creationDate of the list
   * @param completed state of the list
   */
    public TaskList(String title, String color, LocalDateTime creationDate, Boolean completed){
      if(color == null){
        color = "";
      }
      if (color.isBlank() || title == null) {
        throw new IllegalArgumentException("Null or empty parameter");
      }

      this.title = title;
      this.creationDate = creationDate;
      this.completed = completed;
      this.tasks = new ArrayList<>();
      this.color = color;
    }



    /**
     * Returns the title of the list
     *
     * @return the title of the list
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * Sets the the title of the list
     *
     * @param title to be set
     */
    public void setTitle(String title) {
        if (title == null) {
            throw new IllegalArgumentException("Null or empty parameter");
        }
    }

    /**
     * Returns the date and time the list was created
     *
     * @return the date and time the list was created
     */
    public LocalDateTime getCreationDate() {
        return this.creationDate;
    }

    /**
     * Returns {@code true} if every task in the list is completed, {@code false} if a
     * minimum of one task is not completed.
     *
     * @return true or false based on the task in the list
     */
    public boolean getCompleted() {
        return this.completed;
    }

    /**
     * Checks if the list has any items that are not completed.
     * If every task in the list is completed the list will be set to completed.
     * Otherwise the list will stay not completed.
     */
    public void toggleCompleted(boolean completed)
    {
        Iterator<Task> it = this.tasks.iterator();
        boolean itemNotCompletedFound = false;
        while (it.hasNext() || !itemNotCompletedFound) {
            if (!it.next().getCompleted()) {
                itemNotCompletedFound = true;
            }
        }

        if (itemNotCompletedFound) {
            this.completed = false;
        } else {
            this.completed = true;
        }
    }

    /**
     * Returns the task list
     *
     * @return the task list
     */
    public ArrayList<Task> getTasks() {
        return this.tasks;
    }

    /**
     * Returns the color of the list as a hex ode
     *
     * @return the color of the list
     */
    public String getColor() {
        return this.color;
    }

    /**
     * Adds a task to the list
     *
     * @param task to be added
     */
    public void addTask(Task task) {
        this.tasks.add(task);
    }

    /**
     * Returns the first task found that has the same title as given in parameter.
     *
     * @param title to search for
     * @return task found with same title
     */
    public Task getTaskByTitle(String title) {
        Task task = null;

        boolean taskFound = false;
        Iterator<Task> it = this.tasks.iterator();
        while (it.hasNext() && !taskFound) {
            task = it.next();
            if (task.getTitle().equals(title)) {
                taskFound = true;
            }
        }

        return task;
    }

    /**
     * Removes a task from the list. If there are multiple task with the same title, no task will
     * be removed and {@code fasle} will be returned.
     *
     * @param title title of the task to be removed
     * @return {@code true} if task was removed, and {@code false} if task was not removed
     */
    public boolean removeTaskByTitle(String title) {
        boolean taskRemoved = false;

        if (this.checkForTask(title)) {
            if (this.checkIfThereExistsMultipleTasksWithSameTitle(title)) {
                taskRemoved = false;
            } else {
                Task task = this.getTaskByTitle(title);
                this.tasks.remove(task);
                taskRemoved = true;
            }
        }

        return taskRemoved;
    }

    /**
     * Checks if there are multiple task with the same title.
     *
     * @param title of the tasks to search for
     * @return {@code true} if there are multiple tasks with the same name,
     * and {@code false} if there is only one task with the title
     */
    private boolean checkIfThereExistsMultipleTasksWithSameTitle(String title) {
        boolean mulitpleTasksFound = false;

        int numberOfTasksFound = 0;
        for (Task task : this.tasks) {
            if (task.getTitle().equals(title)) {
                numberOfTasksFound += 1;
            }
        }

        if (numberOfTasksFound > 1) {
            mulitpleTasksFound = true;
        }

        return mulitpleTasksFound;
    }

    /**
     * Check if there exists a task in the tasklist with the title given as parameter
     *
     * @param title of task to find
     * @return {@code true} if task was found, {@code false} if task was not found
     */
    private boolean checkForTask(String title) {
        boolean taskFound = false;

        Iterator<Task> it = this.tasks.iterator();
        while (it.hasNext() && !taskFound) {
            if (it.next().getTitle().equals(title)) {
                taskFound = true;
            }
        }

        return taskFound;
    }

    /**
     * Returns a list of all tasks that has a tag with the same title as given in parameter
     *
     * @param tagTitle title of the tag to search for
     * @return a list of task with the tag given as parameter.
     */
    public ArrayList<Task> getTasksByTagTitle(String tagTitle) {
        ArrayList<Task> tasksFound = new ArrayList<>();

        for (Task task : this.tasks) {
            if (task.searchTagByTitle(tagTitle)) {
             tasksFound.add(task);
            }
        }

        return tasksFound;
    }

    /**
     * Returns an iterator that iterates over the list.
     *
     * @return an iterator over the list
     */
    public Iterator<Task> getIterator() {
        return this.tasks.iterator();
    }
}
