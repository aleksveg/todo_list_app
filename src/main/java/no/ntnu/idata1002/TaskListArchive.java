package no.ntnu.idata1002;

import java.util.ArrayList;
import java.util.Iterator;


/**
 * This class represents an archive of task lists.
 * The class should be able to hold the number of lists that the given user
 * creates.
 *
 * @author aleksander / gruppe1
 * @version 1.0
 */
public class TaskListArchive {

    private ArrayList<TaskList> taskLists; // variable to hold all the task-lists

    /**
     * Constructor for class TaskListArchive.
     * Checks that no null-objects created.
     */
    public TaskListArchive() {
        taskLists = new ArrayList<>(); // if valid input entered, create new list

    }

    /**
     * Returns the first TaskList with the same title
     * as given in parameter
     *
     * @param title of the TaskList to look for
     * @return A TaskList with the same title as given in
     * parameter. If no TaskList was found, {@code null}
     * will be returned.
     */
    public TaskList getTaskListByName(String title) {
        TaskList taskListFound = null;

        Iterator<TaskList> it = this.taskLists.iterator();
        while (it.hasNext() && taskListFound == null) {
            TaskList taskList = it.next();
            if (taskList.getTitle().equals(title)) {
                taskListFound = taskList;
            }
        }

        return taskListFound;
    }

    /**
     * This method allows the user to add a new task list to the application
     *
     * @param taskList - the task list to be added
     */
    public void addList(TaskList taskList) {
        if (taskList == null) { // if no input throw an error...
            throw new IllegalArgumentException("Invalid input"); // the error to be thrown
        } else {
            taskLists.add(taskList);
        }
    }

    /**
     * This method allows the user to remove a list from the list archive.
     *
     * @ param taskListTitle - the title of the task list to search for.
     */
    public boolean removeList(String taskListTitle) {
        Iterator<TaskList> it = taskLists.iterator(); // creates an iterator object for the task list
        boolean listFound = false; // variable to check if what we searched for is found
        while (it.hasNext() && !listFound) {
            TaskList t = it.next(); // the next element in the ArrayList
            if (t.getTitle().equalsIgnoreCase(taskListTitle)) { // see if the elements title matches the title to search for
                this.taskLists.remove(t); // if match . remove it from the list
                listFound = true; // listFound set to true to stop the search
            } else {
                System.out.println("No list found"); // no list found in the list archive
                listFound = false;
            }
        }
        return listFound;
    }

    /**
     * Returns the list archive to the user.
     * @return taskLists - all the task lists in the archive to be returned.
     */
    public ArrayList<TaskList> getArchive() {
        return this.taskLists; // the set of lists to be returned
    }

    /**
     * Returns iterator object for the task list archive.
     * @return iterator - iterator to be returned.
     */
    public Iterator<TaskList> getIterator() {
        return this.taskLists.iterator(); // iterator object to be returned
    }

    /**
     * Search for a taskList by title provided by the user.
     * If the title provided by the user via parameter matches the title for an existing task list,
     * return the task list that is matching.
     * @param title - the title to search for
     * @return taskList - the task list to return if match found.
     */
    public TaskList searchTaskListByTitle(String title) {
        Iterator<TaskList> it = getIterator(); // get the iterator for the list
        TaskList taskList = null; // variable to store the found list if there is a match , initialized to null
        boolean titleFound = false; // boolean value to stop the search if a match found
        while(it.hasNext() && !titleFound) { // if the list has a next item and no match yet
            TaskList t = it.next(); // the next element in the list
            if(t.getTitle().equalsIgnoreCase(title)){ // if the searched title matches a title in the list
                taskList = t; // assign the object in the list to the variable to store it
                titleFound = true; // change the value of the search
            } else {
                titleFound = false; // no match found and
            }
        }
        return taskList; // return the taskList
    }


}
