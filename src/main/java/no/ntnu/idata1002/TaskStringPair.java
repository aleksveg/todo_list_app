package no.ntnu.idata1002;

/**
 *Represents a pair consisting of a single Task, and a String.
 * Intended to facilitate the use of a AddTaskWindowGUI's setResultConverter() method,
 * to return both the newly generated Task, and the name of theTaskList that Task
 * should be added to.
 */
public class TaskStringPair {
  private Task task;
  private String taskListName;

  /**
   * Creates a new TaskTaskListPair object.
   * @param t The Task object in the pair.
   * @param tl The TaskList object in the pair.
   */
  public TaskStringPair(Task t, String tl){
    this.task = t;
    this.taskListName = tl;
  }

  /**
   * Returns the object's task.
   * @return the object's task.
   */
  public Task getTask() {
    return task;
  }

  /**
   * Returns the object's taskList.
   * @return The object's taskList.
   */
  public String getTaskListName() {
    return taskListName;
  }

  /**
   * Sets the object's task.
   * @param task The new task.
   */
  public void setTask(Task task) {
    this.task = task;
  }

  /**
   * Sets the object's taskList.
   * @param taskListName The new taskList.
   */
  public void setTaskList(String taskListName) {
    this.taskListName = taskListName;
  }
}
