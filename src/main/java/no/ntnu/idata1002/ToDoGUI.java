package no.ntnu.idata1002;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;

import java.time.LocalDateTime;
import java.util.*;


/**
 * Creates the GUI
 *
 * @author: Thomas/gruppe1
 * @version 1.0
 *
 */
public class ToDoGUI extends Application implements EventHandler {
    /**
     * Holds all the TaskLists the application might have.
     */
    private TaskListArchive taskListArchive;
    /**
     * A HashMap used to help get the selected taskList.
     */
    private HashMap<String, TaskList> taskListHashMap;
    //Need both a TaskList and an ObservableArrayList, as the latter does not have the
    //ability to get the title of the TaskList it is observing.
    /**
     * The taskList that si currently selected by the user.
     */
    private TaskList selectedTaskList;
    /**
     * An observableList used to "Wrap" the objects in selectedTaskList to enable the link
     * between the TableView and the taskList.
     */
    private ObservableList<Task> selectedTaskListWrapper;
    /**
     * Used to display the existing tasks in the selected taskList.
     */
    private TableView<Task> taskTable;

    /**
     * The root pane of the application.
     */
    private BorderPane root;
    // The container that displays all lists as well as buttons for actions
    // you can do to the lists
    /**
     * The left container of the application's root pane.
     */
    private VBox leftContainer;
    // Holds the calender
    /**
     The application's calendar view.
     */
    private CalenderGui calender;
    // Holds the list view
    /**
     * The root pane's left container. Hold any taskLists the application might have.
     */
    private VBox centerContainer;

    // Styling string for components
    /**
     * Styling for buttons.
     * */
    private final static String buttonStyle = "-fx-background-color:#800000;-fx-text-fill:#fff";
    /**
     * Styling for the mainBackground.
     */
    private final static String mainBackground = "-fx-background-color:#d9d9d9";
    /**
     * Styling for background.
     */
    private final static String background = "-fx-background-color:#f2f2f2";
    /**
     * Styling for spacing between elements.
     */
    private final static Insets spacing = new Insets(20);

  /**
   * Handles writing to and reading from xml document to save/load tasks.
   */
  XMLManager xmlManager;
    @Override
    public void handle(Event event) {

    }

    public static void main(String[] args) {
        System.out.println("Starting...");
        //ToDoGUI gui = new ToDoGUI();
        launch(args);
        //System.out.println("Done");
    }

    /**
     * Empty constructor for the ToDoGUI class.
     */
    public ToDoGUI(){
    }

    /**
     * Handled initialization of fields.
     */
    private void initialize(){

        xmlManager = new XMLManager("src/main/resources/savedTasks.xml");
        taskListArchive = this.xmlManager.loadXMLDocument();
        taskListHashMap = new HashMap<String, TaskList>();
        selectedTaskList = null;
        selectedTaskListWrapper = null;
        //taskTable = null;
        taskTable = new TableView();
        taskTable.setMinHeight(534);
        this.calender = new CalenderGui(this);


    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        //Populating taskListArchive with dummy objects.
        initialize();
        //populateTaskListArchive();
        this.root = new BorderPane();
        root.setStyle(mainBackground);
        //Creating border style for containers.

        //Creating and adding top container.
        VBox topContainer = createTopContainer();
        this.root.setTop(topContainer);

        //Creating and adding left container.
        this.leftContainer = createLeftContainer();
        this.root.setLeft(leftContainer);
        BorderPane.setMargin(leftContainer, new Insets(20, 0, 20, 20));

        //Creating and adding centerContainer
        this.centerContainer = createCenterContainer(primaryStage);
        this.root.setCenter(centerContainer);
        BorderPane.setMargin(centerContainer, spacing);

        // Create and add bottom container
        this.root.setBottom(createBottomContainer());


        //Creating the scene, adding root node, setting size.
        Scene scene = new Scene(this.root, 1100, 750);
        //leftContainer.prefWidthProperty().bind(primaryStage.widthProperty().multiply(0.2));
        primaryStage.setScene(scene);
        primaryStage.setTitle("ToDoList");
        primaryStage.show();

    }


    /**
     * Creates the bottom container. Returns a HBox for use in the main window
     * of the application.
     *
     * @return a HBox for use in the main window of the application.
     */
    private HBox createBottomContainer() {
        HBox hbox = new HBox();

        // Set styling for hbox
        hbox.setPadding(new Insets(10));
        hbox.setAlignment(Pos.CENTER);
        hbox.setStyle(background);

        Label label = new Label("@ 2021 - Gruppe 1, IDATA1002. All rights reserved");
        label.setFont(new Font(10));
        hbox.getChildren().add(label);

        return hbox;
    }

    /**
     * Handles creation and population of the application menu
     */
    private MenuBar createMenuBar(){
      MenuBar menuBar = new MenuBar(); //Menu bar to hold all menus.

        //File menu
        Menu menuFile = new Menu("Fil");
        MenuItem saveFile = new MenuItem("Lagre XML");
        MenuItem exitFile = new MenuItem("Avslutt");

        saveFile.setOnAction(actionEvent -> {
            this.xmlManager.generateXMLDocument(this.taskListArchive);
        });

        exitFile.setOnAction(actionEvent -> this.showExitConfirmationDialog());
        menuFile.getItems().addAll(saveFile, exitFile);


      //Adding menus to menuBar
      menuBar.getMenus().addAll(menuFile);

      return menuBar;
    }

    /**
     * Shows a dialog asking the user if they want to exit the application.
     * If so closes the application
     */
    private void showExitConfirmationDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Konfirmasjon");
        alert.setHeaderText("Avslutt applikajon");
        alert.setContentText("Vil du virkelig avslutte applikasjonen");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            Platform.exit();
        }
    }

    /**
     * Creates, populates and returns the topContainer VBox object,
     * including MenuBar and listView/calendarView buttons.
     * @return the application's topContainer VBox
     */
    private VBox createTopContainer()
    {

        VBox tc = new VBox(); //Main vertical box
        //Creating menuBar, and adding it to the main vertical box.
        MenuBar menuBar = createMenuBar();
        tc.getChildren().add(menuBar);
        tc.getChildren().add(new Separator());

        HBox viewBox = new HBox(); //Horizontal box for holding buttons to switch between views.
        //Creating view buttons, and adding event listeners.
        viewBox.setPadding(new Insets(10, 20, 10, 40));
        viewBox.setStyle(background);
        viewBox.setSpacing(5);

        Button goToListViewButton = new Button("Liste vindu");
        goToListViewButton.setStyle(buttonStyle);
        goToListViewButton.setCursor(Cursor.HAND);
        goToListViewButton.setOnAction(actionEvent ->
        {
            root.setCenter(this.centerContainer);
        });

        Button goToCalendarViewButton = new Button("Kalender vindu");
        goToCalendarViewButton.setStyle(buttonStyle);
        goToCalendarViewButton.setCursor(Cursor.HAND);
        goToCalendarViewButton.setOnAction(actionEvent ->
        {
            VBox calenderView = this.calender.displayCalender(this.selectedTaskList);
            this.root.setCenter(calenderView);
            BorderPane.setMargin(calenderView, spacing);
        });

        //Adding viewButtons to viewBox
        viewBox.getChildren().addAll(goToListViewButton, goToCalendarViewButton);
        //Adding viewBox to tc.
        tc.getChildren().add(viewBox);

        return tc;
    }

    /**
     * Creates, populates and returns the leftContainer VBox object.
     *
     * @return The application's leftContainer VBox.
     */
    private VBox createLeftContainer()
    {
        VBox lc = new VBox();
        // Set styles for pane
        lc.setMinWidth(130);
        lc.setPadding(new Insets(20,20,20,20));
        lc.setStyle(background);

        // Create buttons for the list menu.
        HBox buttonList = this.createButtonsForLeftContainer();
        buttonList.setPadding(new Insets(0, 0, 20, 0));
        // Add buttons to the left container
        lc.getChildren().add(buttonList);

        ArrayList<Label> taskListLabels = generateLabelsList(taskListArchive.getArchive());
        //Create a HashMap with TaskLists as values, and Strings as keys,
        //so the text of the clicked label can be used to pull up info
        //on the relevant TaskList.
        populateTaskListHashMap(taskListArchive.getArchive());

        //Adds the Labels to lc.children, so they can be displayed.
        Label l; //To store the label while working on it.
        Iterator<Label> it = taskListLabels.iterator();
        while(it.hasNext())
        {
            l = it.next();
            l.setCursor(Cursor.HAND);
            //finalLabelText should be treated as final, so as not to
            //cause issues with lambda expression.
            String finalLabelText = l.getText();
            l.setOnMouseClicked(mouseEvent ->
            {
                String taskListInfo = taskListHashMap.get(finalLabelText).getTitle() +
                                      " - " + taskListHashMap.get(finalLabelText).getColor();
                System.out.println("Clicked on: " + taskListInfo);
                selectedTaskList = taskListHashMap.get(finalLabelText);
                selectedTaskListWrapper = getSelectedTaskListWrapper();
                taskTable = createTaskTable();
                this.calender.displayCalender(this.selectedTaskList);
            });
            lc.getChildren().add(l);
        }

      return lc;
    }

    /**
     * Creates the buttons for the menu over the list pane
     *
     * @return a HBox with every button in the menu
     */
    private HBox createButtonsForLeftContainer() {

        HBox hbox = new HBox();
        hbox.setSpacing(5);

        // Set style for pane
        hbox.setPadding(new Insets(0, 0, 5, 0));

        Button addListButton = new Button("Legg til liste");
        addListButton.setStyle(buttonStyle);
        addListButton.setCursor(Cursor.HAND);
        addListButton.setOnAction( actionEvent -> {
            System.out.println("Add list button pressed.");
            createAddListWindow();
      });


        Button removeListBtn = new Button("Fjern liste");
        removeListBtn.setStyle(buttonStyle);
        removeListBtn.setCursor(Cursor.HAND);
        removeListBtn.setOnAction(actionEvent -> {
            System.out.println("Remove list button pressed.");
            createRemoveListWindow();
      });

      hbox.getChildren().addAll(addListButton, removeListBtn);

      return hbox;
    }

    /**
     * Creates a pop-up box for removing a TaskList from the TaskListArchive
     */
    private void createRemoveListWindow() {
      RemoveTaskListWindowGUI window = new RemoveTaskListWindowGUI(taskListArchive);

      Optional<TaskList> result = window.showAndWait();

      if (result.isPresent()) {
          TaskList taskListToRemove = result.get();
          removeTaskList(taskListToRemove);
      }
    }

    /**
     * Removes a task list from the application
     *
     * @param taskList The TaskList to be removed
     */
    private void removeTaskList(TaskList taskList) {

        this.taskListArchive.removeList(taskList.getTitle());
        removeTaskListFromHashMap(taskList);
        this.leftContainer = updateLeftContainer();
        this.root.setLeft(this.leftContainer);
        BorderPane.setMargin(leftContainer, new Insets(20, 0, 20, 20));

    }

    /**
     * Removes a TaskList from the taskListHashMap
     *
     * @param taskList the TaskList to be removed
     */
    private void removeTaskListFromHashMap(TaskList taskList) {
        this.taskListHashMap.remove(taskList.getTitle());
    }

    /**
     * Creates a pop-up box for adding a TaskList to the TaskListArchive
     */
    private void createAddListWindow() {
      AddTaskListWindowGUI window = new AddTaskListWindowGUI();


      Optional<TaskList> result = window.showAndWait();

      if (result.isPresent()) {
          TaskList newTaskList = result.get();
          addTaskList(newTaskList);
      }
    }

    /**
     * Adds a taskList to the application
     *
     * @param taskList the TaskList to be added
     */
    private void addTaskList(TaskList taskList) {
        String taskTitle = taskList.getTitle();
        
        if (this.taskListHashMap.containsKey(taskTitle)) {
            createTaskAlreadyExistsAlert(taskList.getTitle());
        } else {
            this.taskListArchive.addList(taskList);
            addTaskListToHashMap(taskList);
            this.leftContainer = updateLeftContainer();
            this.root.setLeft(this.leftContainer);
            BorderPane.setMargin(leftContainer, new Insets(20, 0, 20, 20));
        }
    }

    /**
     * Creates an alert pop-up telling the user
     * that a TaskList with the title the user typed in
     * already exists
     *
     * @param title The title of the list that already exists
     */
    private void createTaskAlreadyExistsAlert(String title) {
      Alert alert = new Alert(Alert.AlertType.INFORMATION);

      alert.setTitle("Informasjon - Ugyldig input");
      alert.setHeaderText("Det du skrev inn var ugyldig");
      alert.setContentText("En liste med navnet " + title +
              " eksisterer allerede!\nVennligst prøv et nytt navn" +
              " til denne listen");

      alert.showAndWait();
    }

    /**
     * Updates the left container. Used when a change is made to the list of
     * TaskLists
     *
     * @return A VBox-pane with every list
     */
    private VBox updateLeftContainer() {

        VBox vbox = new VBox();

        // Set styles for pane
        vbox.setPadding(new Insets(20,20,20,20));
        vbox.setStyle(background);

        // Add buttons to the menu
        HBox buttonMenu = this.createButtonsForLeftContainer();
        buttonMenu.setPadding(new Insets(0, 0, 20, 0));
        vbox.getChildren().add(buttonMenu);

        List<Label> taskListLabel = this.generateLabelsList(this.taskListArchive.getArchive());

        for (Label l : taskListLabel) {
            //finalLabelText should be treated as final, so as not to
            //cause issues with lambda expression.
            String finalLabelText = l.getText();
            l.setOnMouseClicked(mouseEvent ->
            {
                String taskListInfo = taskListHashMap.get(finalLabelText).getTitle() +
                        " - " + taskListHashMap.get(finalLabelText).getColor();
                System.out.println("Clicked on: " + taskListInfo);
                selectedTaskList = taskListHashMap.get(finalLabelText);
                selectedTaskListWrapper = getSelectedTaskListWrapper();
                taskTable = createTaskTable();
                this.calender.displayCalender(this.selectedTaskList);
            });
            vbox.getChildren().add(l);
            //vbox.getChildren().add(new Separator());
        }

      return vbox;
    }

    /**
     * Generates a list of labels, with text wrapping and correct coloring, based on the
     * TaskLists they represent.
     * @param taskLists List of TaskLists that should be displayed.
     * @return List of Labels representing the TaskLists to be displayed.
     */
    private ArrayList<Label> generateLabelsList(ArrayList<TaskList> taskLists)
    {
      ArrayList<Label> taskListLabels = new ArrayList<>();
      Label l; //To hold the label variable before putting it in the ArrayList.

      for(TaskList tl : taskLists)
      {
          l = new Label(tl.getTitle());
          l.setWrapText(true);
          l.setTextFill(Color.web(tl.getColor()));
          l.setPadding(new Insets(5,5,5,5));
          l.setMinWidth(150);
          l.setFont(new Font("Arial", 16));
          taskListLabels.add(l);
      }

      return taskListLabels;
    }

    /**
     * Generates and returns a HashMap, with TaskLists as values, and Labels representing those
     * TaskLists as keys, based on the TaskListArchive.
     * @param taskLists List of TaskLists that should be displayed.
     * @return HashMap with TaskLists as values, and Labels representing those TaskLists
     * as keys.
     */
    private void populateTaskListHashMap(ArrayList<TaskList> taskLists)
    {
      ArrayList<Label> labels = generateLabelsList(taskLists);

      Label l; //To hold the label variable before putting it in the HashMap
      String s;

      for(TaskList tl : taskLists)
      {
          s = tl.getTitle();
          taskListHashMap.put(s, tl);
      }
    }

    /**
     * Adds a TaskList to the taskListHashMap.
     *
     * @param taskList the TaskList to be added
     */
    private void addTaskListToHashMap(TaskList taskList) {
      String taskListTitle = taskList.getTitle();
      if (!this.taskListHashMap.containsKey(taskListTitle)) {
          this.taskListHashMap.put(taskListTitle, taskList);
      }
    }

    /**
     * Creates and returns the centerContainer VBox object
     * @return The application's centerContainer VBox.
     */
    private VBox createCenterContainer(Stage primaryStage){

        VBox cc = new VBox(); //Main center container

        HBox addHideBox = createCenterMenu();

        cc.getChildren().addAll(addHideBox, new Separator());

        cc.getChildren().add(taskTable);

        return cc;
    }

    /**
     * Creates the menu bar in the top center of the application.
     * Returns a hbox that can be added to the center of the
     * borderPane.
     *
     * @return a hbox to be placed in the center of the BorderPane
     */
    private HBox createCenterMenu() {
        HBox addHideBox = new HBox(); //Container for "Add item" and "Hide completed items" buttons.
        addHideBox.setPadding(new Insets(20, 20, 20, 20));
        addHideBox.setSpacing(5);
        addHideBox.setStyle(background);
        //Creating and adding "Add items" and "Hide completed items" buttons.
        //TODO Add proper functionality to addItems and hideCompletedItems buttons.
        Button addItemButton = new Button("Legg til oppgave");
        addItemButton.setStyle(buttonStyle);
        addItemButton.setCursor(Cursor.HAND);
        addItemButton.setOnAction( actionEvent ->
        {
            System.out.println("Add items clicked");
            //AddTaskWindowGUI addTaskWindowGUI = new AddTaskWindowGUI(taskListArchive.getArchive());
            doAddTask();
        });
        Button deleteItem = new Button("Fjern oppgave");
        deleteItem.setCursor(Cursor.HAND);
        deleteItem.setStyle(buttonStyle);
        deleteItem.setOnAction(actionEvent -> {
            try {
                this.deleteTask(this.taskTable.getSelectionModel().getSelectedItem());
            } catch (Exception e) {
                showPleaseSelectItemDialog();
            }
        });
        Button hideCompletedButton = new Button("Lagre");
        hideCompletedButton.setCursor(Cursor.HAND);
        hideCompletedButton.setStyle(buttonStyle);
        hideCompletedButton.setOnAction( actionEvent ->
        {
            this.xmlManager.generateXMLDocument(this.taskListArchive);
            System.out.println("Item has been saved");
        });

        addHideBox.getChildren().addAll(addItemButton, deleteItem, hideCompletedButton);

        return addHideBox;
    }

    /**
     * Deletes a task from the list the task is in. If no task is given in the
     * parameter, an alert-box will be displayed.
     *
     * @param task The task to be deleted
     */
    private void deleteTask(Task task) {
      if (task == null) {
          throw new IllegalArgumentException();
      }else {
          this.selectedTaskList.removeTaskByTitle(task.getTitle());
          this.selectedTaskListWrapper = getSelectedTaskListWrapper();
          createTaskTable();
      }
    }

    /**
     * Creates an alert-box explaining that the user has to select
     * a task before trying to delete anything
     */
    private void showPleaseSelectItemDialog() {
      Alert alert = new Alert(Alert.AlertType.INFORMATION);
      alert.setTitle("*Informasjon - Slett oppgave");
      alert.setHeaderText("Venligst velg en oppgave du vill slette");
      alert.setContentText("Du må velge en oppgave før du prøver \n" +
              "å slette noe");
      alert.showAndWait();
    }

    /**
     * Handles creation of TableView to display tasks in centerContainer.
     * Uses ToDoGUI's private field selectedTaskList to determine what the table should be based on.
     * @return TableView of tasks to be displayed.
     */
    private TableView<Task> createTaskTable()
    {
      TableColumn<Task, String> taskTitleColumn = new TableColumn<Task, String>("Oppgave");
      taskTitleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));

      TableColumn<Task, LocalDateTime> taskDeadlineColumn = new TableColumn<Task, LocalDateTime>("Deadline");
      taskDeadlineColumn.setCellValueFactory(new PropertyValueFactory<>("deadline"));

      TableColumn<Task, String> taskPriorityColumn = new TableColumn<Task, String>("Prioritet");
      taskPriorityColumn.setCellValueFactory(new PropertyValueFactory<>("priority"));

      TableColumn<Task, String> taskCompletedColumn = new TableColumn<Task, String>("Fullført");
      taskCompletedColumn.setCellValueFactory(new PropertyValueFactory<>("completed"));

      taskTable.getColumns().clear(); //Removes any preexisting columns.
      taskTable.getColumns().addAll(taskTitleColumn, taskDeadlineColumn,
          taskPriorityColumn, taskCompletedColumn);
      taskTable.setItems(selectedTaskListWrapper);

      taskTable.setOnMousePressed(mouseEvent -> {
        if(mouseEvent.isPrimaryButtonDown() && (mouseEvent.getClickCount() == 2)){
          Task selectedTask = taskTable.getSelectionModel().getSelectedItem();
          doShowDetails(selectedTask);
        }
      });

      return taskTable;
    }

    /**
     * Returns the selectedTaskListWrapper
     * @return selectedTaskListWrapper
     */
    private ObservableList<Task> getSelectedTaskListWrapper()
    {
      selectedTaskListWrapper = FXCollections.observableArrayList(selectedTaskList.getTasks());

      return selectedTaskListWrapper;
    }

    /**
     * Creates some TaskLists with Tasks.
     * NB! FOR TESTING PURPOSES ONLY!!!
     */
    private void populateTaskListArchive()
    {
      //Creating dummy time
      LocalDateTime ltd = LocalDateTime.of(2021, 5,22, 15,00);

      //Creating and populating taskLists, as empty task lists cannot be added to the archive.
      TaskList taskList1 = new TaskList("First taskList", "Red");
      taskList1.addTask(new Task("TestTask1-1", "For testing purposes 1-1", "Thomas",
              ltd, 1, false));
      taskList1.addTask(new Task("TestTask1-2", "For testing purposes 1-2", "Thomas",
              ltd, 2, true));


      TaskList taskList2  = new TaskList("Second taskList", "Green");
      taskList2.addTask(new Task("TestTask2-1", "For testing purposes 2-1", "Thomas",
              ltd, 3, true));
      taskList2.addTask(new Task("Testtask2-2", "Fourth taskList", "Thomas",
              ltd, 4, false));

      //Adding tasksLists to archive
      taskListArchive.addList(taskList1);
      taskListArchive.addList(taskList2);
    }

  /**
   * Adds a Task to the specified TaskList via GUI.
   */
  public void doAddTask(){
      TaskDetailsGUI addTaskDialog = new TaskDetailsGUI(taskListArchive.getArchive());
      Optional<TaskStringPair> result = addTaskDialog.showAndWait();
      if(result.isPresent()) {
        TaskStringPair pair = result.get();
        TaskList target = taskListHashMap.get(pair.getTaskListName());
        taskListHashMap.get(pair.getTaskListName()).addTask(pair.getTask());
        taskTable = createTaskTable();
      }
    }

    /**
     * Makes a TaskDetailsGUI-dialog in Edit mode to display and edit the selected task.
     * @param selectedTask The currently selected task.
     */
    public void doShowDetails(Task selectedTask){
      TaskDetailsGUI editTaskDialog = new TaskDetailsGUI(selectedTask);
      Optional<TaskStringPair> result = editTaskDialog.showAndWait();
      if(result.isPresent()) {
        Task resultTask = result.get().getTask();
        //Find selectedTask in the taskList.
        for(Task t : selectedTaskList.getTasks()){
          if(t.equals(selectedTask)){
            t.setTitle(resultTask.getTitle());
            t.setDescription(resultTask.getDescription());
            t.setDeadline(resultTask.getDeadline());
            t.setAuthor(resultTask.getAuthor());
            t.setCompleted(resultTask.getCompleted());
            t.setPriority(resultTask.getPriority());
            taskTable = createTaskTable();
          }
        }
      }
    }
}
