package no.ntnu.idata1002;

import java.time.LocalDateTime;
import javax.xml.transform.OutputKeys;

import org.w3c.dom.*;

import java.io.*;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.xml.parsers.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;


/**
 * Class for handling read/write to xml-file, which will be used to
 * store and generate data for the ToDo-list application.
 * Has a StringBuilder that holds the String that should be written to the XML document.
 */

public class XMLManager {
  /**
   * Path to the file that should be written to. As there is only one
   * archive, this variable does not need to change.
   */
  private String targetXMLPath;
  //private DocumentBuilderFactory factory;
  /**
   * For building the xml document.
   */
  private DocumentBuilder docBuilder;

  /**
   * Represents the document that should be created by the XMLManager.
   */
  private Document doc;

  private Logger logger = Logger.getLogger(getClass().toString());

  /**
   * Constructor for the XMLManager class
   * @param path The path of the XML file that the object should write to.
   */
  public XMLManager(String path){
    this.targetXMLPath = path;
    try {
      docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    } catch (ParserConfigurationException e) {
      e.printStackTrace();
    }
  }


  /**
   * Creates a xml document in the path specified by targetXMLPath,
   * based on the state of the applications taskListArchive.
   */
  public void generateXMLDocument(TaskListArchive taskListArchive){
    Document doc = docBuilder.newDocument();
    //Create taskListArchive element
    Element rootElement = doc.createElement("taskListArchive");
    doc.appendChild(rootElement);
    //rootElement.appendChild(doc.createTextNode("A"));
    //rootElement.appendChild(doc.createElement("B"));


    for(TaskList taskList : taskListArchive.getArchive()){
      Element taskListElement = doc.createElement("taskList");

      //List title element
      Element taskListTitleEle = doc.createElement("taskListTitle");
      taskListTitleEle.appendChild(doc.createTextNode(taskList.getTitle()));
      taskListElement.appendChild(taskListTitleEle);

      //Completed element
      Element taskListCompletedEle = doc.createElement("taskListCompleted");
      taskListCompletedEle.appendChild(doc.createTextNode(String.valueOf(taskList.getCompleted())));
      taskListElement.appendChild(taskListCompletedEle);

      //Color element
      Element taskListColorEle = doc.createElement("taskListColor");
      taskListColorEle.appendChild(doc.createTextNode(taskList.getColor()));
      taskListElement.appendChild(taskListColorEle);

      //Creation date elements
      Element taskListCreationDateEle = doc.createElement("taskListCreationDate");
      Element creationDateYear = doc.createElement("taskListCreationYear");
      creationDateYear.appendChild(doc.createTextNode((String.valueOf(taskList.getCreationDate().getYear()))));
      taskListCreationDateEle.appendChild(creationDateYear);

      Element creationDateMonth = doc.createElement("taskListCreationMonth");
      creationDateMonth.appendChild(doc.createTextNode(String.valueOf(taskList.getCreationDate().getMonthValue())));
      taskListCreationDateEle.appendChild(creationDateMonth);

      Element creationDateDay = doc.createElement("taskListCreationDay");
      creationDateDay.appendChild(doc.createTextNode(String.valueOf(taskList.getCreationDate().getDayOfMonth())));
      taskListCreationDateEle.appendChild(creationDateDay);

      Element creationDateHour = doc.createElement("taskListCreationHour");
      creationDateHour.appendChild(doc.createTextNode(String.valueOf(taskList.getCreationDate().getHour())));
      taskListCreationDateEle.appendChild(creationDateHour);

      Element creationDateMinute = doc.createElement("taskListCreationMinute");
      creationDateMinute.appendChild(doc.createTextNode(String.valueOf(taskList.getCreationDate().getMinute())));
      taskListCreationDateEle.appendChild(creationDateMinute);

      taskListElement.appendChild(taskListCreationDateEle);

      //Iterating through tasks
      for(Task task : taskList.getTasks()){
        Element taskElement = doc.createElement("task");
        //Task title element
        Element taskTitleEle = doc.createElement("taskTitle");
        taskTitleEle.appendChild(doc.createTextNode(task.getTitle()));
        taskElement.appendChild(taskTitleEle);

        //Task description element
        Element taskDescription = doc.createElement("taskDescription");
        taskDescription.appendChild(doc.createTextNode(task.getDescription()));
        taskElement.appendChild(taskDescription);

        //Task creationDate element
        Element taskCreationDateEle = doc.createElement("taskCreationDate");
        Element taskCreationDateYear = doc.createElement("taskCreationDateYear");
        taskCreationDateYear.appendChild(doc.createTextNode(String.valueOf(task.getCreationDate().getYear())));
        taskCreationDateEle.appendChild(taskCreationDateYear);

        Element taskCreationDateMonth = doc.createElement("taskCreationDateMonth");
        taskCreationDateMonth.appendChild(doc.createTextNode(String.valueOf(task.getCreationDate().getMonthValue())));
        taskCreationDateEle.appendChild(taskCreationDateMonth);

        Element taskCreationDateDay = doc.createElement("taskCreationDateDay");
        taskCreationDateDay.appendChild(doc.createTextNode(String.valueOf(task.getCreationDate().getDayOfMonth())));
        taskCreationDateEle.appendChild(taskCreationDateDay);

        Element taskCreationDateHour = doc.createElement("taskCreationDateHour");
        taskCreationDateHour.appendChild(doc.createTextNode(String.valueOf(task.getCreationDate().getHour())));
        taskCreationDateEle.appendChild(taskCreationDateHour);

        Element taskCreationDateMinute = doc.createElement("taskCreationDateMinute");
        taskCreationDateMinute.appendChild(doc.createTextNode(String.valueOf(task.getCreationDate().getMinute())));
        taskCreationDateEle.appendChild(taskCreationDateMinute);

        taskElement.appendChild(taskCreationDateEle);

        //Task deadline.
        Element taskDeadlineEle = doc.createElement("taskDeadline");
        Element taskDeadlineYear = doc.createElement("taskDeadlineYear");
        taskDeadlineYear.appendChild(doc.createTextNode(String.valueOf(task.getDeadline().getYear())));
        taskDeadlineEle.appendChild(taskDeadlineYear);

        Element taskDeadlineMonthEle = doc.createElement("taskDeadlineMonth");
        taskDeadlineMonthEle.appendChild(doc.createTextNode(String.valueOf(task.getDeadline().getMonthValue())));
        taskDeadlineEle.appendChild(taskDeadlineMonthEle);

        Element taskDeadlineDayEle = doc.createElement("taskDeadlineDay");
        taskDeadlineDayEle.appendChild(doc.createTextNode(String.valueOf(task.getDeadline().getDayOfMonth())));
        taskDeadlineEle.appendChild(taskDeadlineDayEle);

        Element taskDeadlineHourEle = doc.createElement("taskDeadlineHour");
        taskDeadlineHourEle.appendChild(doc.createTextNode(String.valueOf(task.getDeadline().getHour())));
        taskDeadlineEle.appendChild(taskDeadlineHourEle);

        Element taskDeadlineMinuteEle = doc.createElement("taskDeadlineMinute");
        taskDeadlineMinuteEle.appendChild(doc.createTextNode(String.valueOf(task.getDeadline().getMinute())));
        taskDeadlineEle.appendChild(taskDeadlineMinuteEle);

        taskElement.appendChild(taskDeadlineEle);

        //Task author element
        Element taskAuthorEle = doc.createElement("taskAuthor");
        taskAuthorEle.appendChild(doc.createTextNode(task.getAuthor()));
        taskElement.appendChild(taskAuthorEle);

        //Task completed element
        Element taskCompletedEle = doc.createElement("taskCompleted");
        taskCompletedEle.appendChild(doc.createTextNode(String.valueOf(task.getCompleted())));
        taskElement.appendChild(taskCompletedEle);

        //Task priority element
        Element taskPriorityEle = doc.createElement("taskPriority");
        taskPriorityEle.appendChild(doc.createTextNode(String.valueOf(task.getPriority())));
        taskElement.appendChild(taskPriorityEle);

        //Task recurring element
        Element taskRecurringEle = doc.createElement("taskRecurring");
        taskRecurringEle.appendChild(doc.createTextNode(String.valueOf(task.getRecurring())));
        taskElement.appendChild(taskRecurringEle);

        taskListElement.appendChild(taskElement);
      }

      rootElement.appendChild(taskListElement);
    }

    try {
      Transformer transformer = TransformerFactory.newInstance().newTransformer();
      transformer.setOutputProperty(OutputKeys.INDENT, "yes");
      transformer.setOutputProperty(OutputKeys.METHOD, "xml");
      transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
      DOMSource source = new DOMSource(doc);
      StreamResult result = new StreamResult(new File(targetXMLPath));
      transformer.transform(source, result);
    } catch (TransformerConfigurationException e) {
      e.printStackTrace();
    } catch (TransformerException e) {
      e.printStackTrace();
    }
  }


  /**
   * Handles parsing/loading of xml file.
   */
  public TaskListArchive loadXMLDocument(){
    TaskListArchive newTaskListArchive = new TaskListArchive();
    try{
      //Creating TaskListArchive to hold task data.


      //Getting root element of the xml file (taskListArchive)
      Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(targetXMLPath);
      System.out.println("Root element: " + doc.getDocumentElement().getNodeName());
      NodeList taskLists = doc.getElementsByTagName("taskList");
      System.out.println("--------------------------");

      //Iterating through child nodes of taskListArchive (taskLists)
      for (int x = 0; x < taskLists.getLength(); x++){
        Node taskListNode = taskLists.item(x);
        TaskList newTaskList;

        if(taskListNode.getNodeType() == Node.ELEMENT_NODE){
          Element taskListElement = (Element) taskListNode;

          //getting taskListTitle, taskListCompleted and taskListColor
          String taskListTitle = getFirstElementByTag(taskListElement, "taskListTitle");
          String taskListColor = getFirstElementByTag(taskListElement, "taskListColor");
          String taskListCompleted = getFirstElementByTag(taskListElement, "taskListCompleted");

          //Creating boolean based on taskListCompleted element
          Boolean taskListCompletedB;
          if(taskListCompleted.equals("true"))
            taskListCompletedB = true;
          else
            taskListCompletedB = false;

          //Getting elements of taskListCreationDate. Note that we don't need to go further
          //down in the element hierarchy, as we can get any elements in the taskList we are
          //currently on.
          String taskListCreationYear = getFirstElementByTag(taskListElement, "taskListCreationYear");
          String taskListCreationMonth = getFirstElementByTag(taskListElement, "taskListCreationMonth");
          String taskListCreationDay = getFirstElementByTag(taskListElement, "taskListCreationDay");
          String taskListCreationHour = getFirstElementByTag(taskListElement, "taskListCreationHour");
          String taskListCreationMinute = getFirstElementByTag(taskListElement, "taskListCreationMinute");


          //Creating LocalDateTime based on parsed data
          LocalDateTime taskListCreationDate = LocalDateTime.of(Integer.parseInt(taskListCreationYear),
            Integer.parseInt(taskListCreationMonth), Integer.parseInt(taskListCreationDay),
            Integer.parseInt(taskListCreationHour), Integer.parseInt(taskListCreationMinute));


          //Creating taskList based on currently gathered info.
          newTaskList = new TaskList(taskListTitle, taskListColor, taskListCreationDate, taskListCompletedB);
          //Iterating through tasks in current taskList
          NodeList tasksInList = doc.getElementsByTagName("task");
          for(int y = 0; y < tasksInList.getLength(); y++ ){
            Node taskNode = tasksInList.item(y);
            Task newTask = null;

            if(taskNode.getNodeType() == Node.ELEMENT_NODE &&
              taskNode.getParentNode() == taskListElement){
              Element taskElement = (Element) taskNode;

              //Getting taskTitle, taskDescription, taskAuthor, taskPriority,
              //taskCompleted, taskRecurring
              String taskTitle = getFirstElementByTag(taskElement, "taskTitle");
              String taskDescription = getFirstElementByTag(taskElement, "taskDescription");
              String taskAuthor = getFirstElementByTag(taskElement, "taskAuthor");
              String taskPriority = getFirstElementByTag(taskElement, "taskPriority");
              String taskCompleted = getFirstElementByTag(taskElement, "taskCompleted");
              String taskRecurring = getFirstElementByTag(taskElement, "taskRecurring");


              //Creating boolean based on taskCompleted
              Boolean taskCompletedB;
              if(taskCompleted.equals("true"))
                taskCompletedB = true;
              else
                taskCompletedB = false;

              //Creating boolean based on taskRecurring
              Boolean taskRecurringB;
              if(taskRecurring.equals("true"))
                taskRecurringB = true;
              else
                taskRecurringB = false;

              //Getting elements of taskCreationDate
              String taskCreationDateYear = getFirstElementByTag(taskElement, "taskCreationDateYear");
              String taskCreationDateMonth = getFirstElementByTag(taskElement, "taskCreationDateMonth");
              String taskCreationDateDay = getFirstElementByTag(taskElement, "taskCreationDateDay");
              String taskCreationDateHour = getFirstElementByTag(taskElement, "taskCreationDateHour");
              String taskCreationDateMinute = getFirstElementByTag(taskElement, "taskCreationDateMinute");

              //Creating LocalDateTime for taskCreationDate based on parsed XML data.
              LocalDateTime taskCreationDate =
                LocalDateTime.of(Integer.parseInt(taskCreationDateYear),
                  Integer.parseInt(taskCreationDateMonth), Integer.parseInt(taskCreationDateDay),
                  Integer.parseInt(taskCreationDateHour), Integer.parseInt(taskCreationDateMinute));

              //Getting elements of taskDeadline
              String taskDeadlineYear = getFirstElementByTag(taskElement, "taskDeadlineYear");
              String taskDeadlineMonth = getFirstElementByTag(taskElement, "taskDeadlineMonth");
              String taskDeadlineDay = getFirstElementByTag(taskElement, "taskDeadlineDay");
              String taskDeadlineHour = getFirstElementByTag(taskElement, "taskDeadlineHour");
              String taskDeadlineMinute = getFirstElementByTag(taskElement, "taskDeadlineMinute");

              //Creating LocalDateTime for taskDeadline based on parsed XML data
              LocalDateTime taskDeadline =
                LocalDateTime.of(Integer.parseInt(taskDeadlineYear),
                  Integer.parseInt(taskDeadlineMonth), Integer.parseInt(taskDeadlineDay),
                  Integer.parseInt(taskDeadlineHour), Integer.parseInt(taskDeadlineMinute));

              //Generate task object based on parsed XML data
              newTask = new Task(taskTitle, taskDescription, taskAuthor,
                taskDeadline, taskCreationDate, Integer.parseInt(taskPriority), taskRecurringB,
                taskCompletedB);

              //Add the newly created task object to the corresponding taskList
              newTaskList.addTask(newTask);
            }
          }
          //Add the newly created taskList to the taskListArchive
          newTaskListArchive.addList(newTaskList);
        }
      }

    } catch (Exception e){
      this.logger.info(e.getMessage());
    }
    return newTaskListArchive;
  }

  /**
   * Returns the first element found by Element.getElementByTagName().
   * Since all element types have unique tags, the first element should be the
   * one you are looking for.
   * @param e The element to search in.
   * @param tag The name of the tag you are searching for.
   * @return The first element returned by getElementsByTagName().
   */
  private String getFirstElementByTag(Element e, String tag){
    return e.getElementsByTagName(tag).item(0).getTextContent();
  }
  

  /**
   * Changes the targetXMLPath, which is the file that the object writes to.
   * @param targetXMLPath The new target file path.
   */
  public void setTargetXMLPath(String targetXMLPath) {
    this.targetXMLPath = targetXMLPath;
  }

  /**
   * Returns the objects targetXMLPath.
   * @return the objects targetXMLPath.
   */
  public String getTargetXMLPath(){
    return this.targetXMLPath;
  }
}
