package no.ntnu.idata1002;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This is a class to perform tests on the Tag class.
 * Both negative and positive tests will be made to ensure that the class is
 * running properly.
 *
 * @author aleksander v.
 * @version 0.1
 */


public class TagTest {


    public TagTest() {

    }


    /**
     * This method should return the title of the tag-object
     * @return title - the title of the tag.
     */
    @Test
    public void testGetTitleWithValidTitle() {
        Tag tag = new Tag("TestTag");
        tag.getTitle();

        assertEquals("TestTag", tag.getTitle());
    }

    /**
     * Test the tag method with empty string as title.
     * If the title is a null-object or is blank, an exception will be thrown.
     * In this case the test only succeeds if the exception is thrown and fails otherwise.
     */
    @Test
    public void testGetTitleWithEmptyStringAsTitle() {
        try {
            Tag tag = new Tag("");
            fail("no success");
        } catch(IllegalArgumentException e) {
            assertTrue(true);
        }

    }

    /**
     * Test the constructor of the tag method with null-object as title.
     * If the title is a null-object or is blank, an exception should be trhown.
     * IN this case the test only succeeds if the exception is thrown and fails otherwise.
     */
    @Test
    public void testGetTitleWithNullAsTitle() {
        try {
            Tag tag = new Tag(null);
            fail("no success");
        } catch(IllegalArgumentException e) {
            assertTrue(true);
        }
    }


}
